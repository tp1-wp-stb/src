<?php
    /*---------------
    [DESC]       : Fonction d'assainissement de donnée (balise, espace vide, slash, char spéciaux)
    [IN : $data] : La donnée à assainir
    [OUT]        : La donnée traitée
    ---------------*/
    function testInput($data) {
        $data = strip_tags($data);
        $data = trim($data);
        $data = addslashes($data); //demande stripslashes() juste avant d'afficher
        $data = htmlspecialchars($data);
        return $data;
    }

    /*---------------
    [DESC]       : Vérifie si la donnée entrée et un chiffre et est entre le min et max fourni
    [IN : $data] : La donnée
    [IN : $min]  : La valeur minimum, défaut PHP_INT_MIN
    [IN : $max]  : La valeur maximum, défaut PHP_INT_MAX
    [OUT]        : True si la donnée est un chiffre entre min et max, sinon False
    ---------------*/
    function estChiffre($data, $min = PHP_INT_MIN, $max = PHP_INT_MAX) {
        return is_numeric($data) && $data>=$min && $data<=$max;
    }

    /*---------------
    [DESC]       : Vérifie si la donnée entrée est une chaine dont la longeur est entre les borne
    [IN : $data] : La donnée
    [IN : $min]  : La longeur minimum, défaut 0
    [IN : $max]  : La longeur maximum, défaut PHP_INT_MAX
    [OUT]        : True si la longeur est entre min et max, sinon False
    ---------------*/
    function estChaine($data, $min = 0, $max = PHP_INT_MAX) {
        return strlen($data)>=$min && strlen($data)<=$max;
    }

    /*---------------
    [DESC]         : Requete qui retourne les infos des 5 meilleurs produits
    [IN : $connBD] : La connexion vers la BD
    [OUT]          : Tableau contenant les infos des produits
    ---------------*/
    function getBest($connBD){
        try {
            $req = $connBD->prepare('SELECT *, Somme/NbVotes AS Note FROM produit WHERE Quantite > 0 AND NbVotes > 0 order by Note DESC limit 5');
            $req->execute();
            $result = $req->fetchAll();
        } catch (PDOException $e) {
            exit("<p>Erreur lors de l'exécution de la requête SQL :</p>\n" . $e->getMessage());
        }
        $req = NULL;
        return $result;
    }

    /*---------------
    [DESC]         : Requete qui retourne les infos des 5 produits ayant les plus grandes quantités en stock
    [IN : $connBD] : La connexion vers la BD
    [OUT]          : Tableau contenant les infos des produits
    ---------------*/
    function getStock($connBD){
        try {
            $req = $connBD->prepare('SELECT * FROM produit order by Quantite DESC limit 5');
            $req->execute();
            $result = $req->fetchAll();
        } catch (PDOException $e) {
            exit("<p>Erreur lors de l'exécution de la requête SQL :</p>\n" . $e->getMessage());
        }
        $req = NULL;
        return $result;
    }

    /*---------------
    [DESC]         : Requete qui retourne les infos du produit de l'id demandé
    [IN : $connBD] : La connexion vers la BD
    [IN : $id]     : id du produit à retourner
    [OUT]          : Tableau contenant les infos du produit
    ---------------*/
    function getProduit($connBD, $id){
        try {
            $req = $connBD->prepare("SELECT * FROM produit WHERE IdProduit='$id'");
            $req->execute();
            $result = $req->fetchAll();
        } catch (PDOException $e) {
            exit("<p>Erreur lors de l'exécution de la requête SQL :</p>\n" . $e->getMessage());
        }
        $req = NULL;
        return $result;
    }

    /*---------------
    [DESC]           : Vérifie si un courriel est déjà utilisé
    [IN : $connBD]   : La connexion vers la BD
    [IN : $courriel] : Le courriel a vérifié 
    [OUT]            : True s'il est disponible, false sinon
    ---------------*/
    function estCourrielDispo($connBD, $courriel){
        try {
            $req = $connBD->prepare("SELECT * FROM utilisateur WHERE Courriel='$courriel'");
            $req->execute();
            $result = $req->fetchAll();
        } catch (PDOException $e) {
            exit("<p>Erreur lors de l'exécution de la requête SQL :</p>\n" . $e->getMessage());
        }
        $req = NULL;
        return count($result)==0;
    }

    /*---------------
    [DESC]            : Donne le nombre de page qu'une recherche obtient
    [IN : $connBD]    : La connexion vers la BD
    [IN : $recherche] : L'élément recherché
    [IN : $min]       : Le prix minimun d'un produit
    [IN : $max]       : Le prix maximum d'un produit
    [OUT]             : Le nombre de page
    ---------------*/
    function nbPage($connBD, $recherche, $min="", $max=""){
        try {
            $requete = "SELECT * FROM produit WHERE (Titre LIKE '%$recherche%' OR `Description` LIKE '%$recherche%')";
            if(!empty($min)){
                $requete.= " AND Prix >= $min";
            }
            if(!empty($max)){
                $requete.= " AND Prix <= $max";
            }
            $req = $connBD->prepare($requete);
            $req->execute();
            $result = $req->fetchAll();
        } catch (PDOException $e) {
            exit("<p>Erreur lors de l'exécution de la requête SQL :</p>\n" . $e->getMessage());
        }
        $req = NULL;
        return ceil(count($result)/10);
    }

    /*---------------
    [DESC]            : Recherche les produits dont l'élément recherché est dans le titre ou la description
    [IN : $connBD]    : La connexion vers la BD
    [IN : $recherche] : L'élément recherché
    [IN : $decalage]  : Décalage de l'affichage
    [IN : $min]       : Le prix minimun d'un produit
    [IN : $max]       : Le prix maximum d'un produit
    [OUT]             : Tableau contenant les infos des produit contentant l'élément recherché
    ---------------*/
    function rechercheProduit($connBD, $recherche, $decalage=0, $min="", $max=""){
        try {
            $requete = "SELECT * FROM produit WHERE (Titre LIKE '%$recherche%')";
            if(!empty($min)){
                $requete.= " AND Prix >= $min";
            }
            if(!empty($max)){
                $requete.= " AND Prix <= $max";
            }
            $requete.=" LIMIT $decalage, 10";
            $req = $connBD->prepare($requete);
            $req->execute();
            $result = $req->fetchAll();
        } catch (PDOException $e) {
            exit("<p>Erreur lors de l'exécution de la requête SQL :</p>\n" . $e->getMessage());
        }
        $req = NULL;
        return $result;
    }

    /*---------------
    [DESC]         : Récupère les achats d'un utilisateur
    [IN : $connBD] : La connexion vers la BD
    [IN : $id]     : id de l'utilisateur
    [OUT]          : Tableau contenant les infos des achats de l'utilisateurs donné
    ---------------*/
    function getAchats($connBD, $id){
        try {
            $req = $connBD->prepare("SELECT * FROM achat INNER JOIN produit ON FkProduit = IdProduit INNER JOIN utilisateur ON FkUtilisateur = IdUtilisateur WHERE FkUtilisateur = '$id' ORDER BY Date DESC");
            $req->execute();
            $result = $req->fetchAll();
        } catch (PDOException $e) {
            exit("<p>Erreur lors de l'exécution de la requête SQL :</p>\n" . $e->getMessage());
        }
        $req = NULL;
        return $result;
    }

    /*---------------
    [DESC]         : Retourne la quantité restante d'un produit dans l'inventaire
    [IN : $connBD] : La connexion vers la BD
    [IN : $id]     : id du produit
    [OUT]          : quantité restante du produit
    ---------------*/
    function getQuantiteRestante($connBD, $id) {
        try {
            $req = $connBD->prepare("SELECT Quantite FROM produit WHERE IdProduit = '$id'");
            $req->execute();
            $result = $req->fetchAll();
        } catch(PDOException $e) {
            exit("<p>Erreur lors de l'exécution de la requête SQL :</p>\n" . $e->getMessage());
        }
        $req = NULL;
        return $result[0];
    }

    /*---------------
    [DESC]         : Retourne la quantité restante d'un produit dans l'inventaire
    [IN : $connBD] : La connexion vers la BD
    [IN : $id]     : id du produit
    [IN : $quantite] : quantité du produit commander
    [OUT]          : true si réussi, false sinon
    ---------------*/
    function soustraireQuantite($connBD, $id, $quantite) {
        try {
            $req = $connBD->prepare("UPDATE produit SET Quantite = Quantite - $quantite WHERE IdProduit = $id");
            $reussite = $req->execute();
        } catch(PDOException $e) {
            exit("<p>Erreur lors de l'exécution de la requête SQL :</p>\n" . $e->getMessage());
        }
        $req = NULL;
        return $reussite;
    }

        /*---------------
    [DESC]         : Ajoute un achat a la base de donnée
    [IN : $connBD] : La connexion vers la BD
    [IN : $date] : date d'achat
    [IN : $idUtil] : id de l'utilisateur
    [IN : $quantite] : quantité du produit commander
    [IN : $idProduit] : id du produit
    [OUT]          : true si réussi, false sinon
    ---------------*/
    function ajouterAchat($connBD, $date, $quantite, $idUtil, $idProduit) {
        try {
            //$newDate = strtotime($date);
            $req = $connBD->prepare("INSERT INTO achat (`Date`, Quantite, FkUtilisateur, FkProduit)
            VALUES ('$date', '$quantite', '$idUtil', '$idProduit')");
            $reussite = $req->execute();
        } catch(PDOException $e) {
            exit("<p>Erreur lors de l'exécution de la requête SQL :</p>\n" . $e->getMessage());
        }
        $req = NULL;
        return $reussite;
    }

    /*---------------
    [DESC]         : Ajout d'un produit dans la BD
    [IN : $connBD] : La connexion vers la BD
    [IN : $titre]  : Valeur du champs Titre
    [IN : $desc]   : Valeur du champs Description
    [IN : $prix]   : Valeur du champs Prix
    [IN : $qte]    : Valeur du champs Quantite
    [IN : $somme]  : Valeur du champs Somme
    [IN : $vote]   : Valeur du champs NbVotes
    [OUT]          : L'Id de l'élément ajouté
    ---------------*/
    function ajoutProduit($connBD, $titre, $desc, $prix, $qte, $somme, $vote){
        try {
            $req = $connBD->prepare("INSERT INTO produit (Titre, `Description`, Prix, Quantite, Somme, NbVotes) VALUES ('$titre', '$desc', '$prix', '$qte', '$somme', '$vote')");
            $req->execute();
            $last_id = $connBD->lastInsertId();
        } catch (PDOException $e) {
            exit("<p>Erreur lors de l'exécution de la requête SQL :</p>\n" . $e->getMessage());
        }
        $req = NULL;
        return $last_id;
    }

    /*---------------
    [DESC]         : Modification d'un produit dans la BD à partir de son ID
    [IN : $connBD] : La connexion vers la BD
    [IN : $id]     : L'id du produit
    [IN : $titre]  : Nouvelle valeur du champs Titre
    [IN : $desc]   : Nouvelle valeur du champs Description
    [IN : $prix]   : Nouvelle valeur du champs Prix
    [IN : $qte]    : Nouvelle valeur du champs Quantite
    [IN : $somme]  : Nouvelle valeur du champs Somme
    [IN : $vote]   : Nouvelle valeur du champs NbVotes
    [OUT]          : True si réussit, false sinon
    ---------------*/
    function modifProduit($connBD, $id,  $titre, $desc, $prix, $qte, $somme, $vote){
        try {
            $req = $connBD->prepare("UPDATE produit SET Titre = '$titre', `Description` = '$desc', Prix = '$prix', Quantite = '$qte', Somme = '$somme', NbVotes = '$vote' WHERE IdProduit = '$id'");
            $reussite = $req->execute();
        } catch (PDOException $e) {
            exit("<p>Erreur lors de l'exécution de la requête SQL :</p>\n" . $e->getMessage());
        }
        $req = NULL;
        return $reussite;
    }

    /*---------------
    [DESC]         : Ajoute la valeur d'un vote à la note d'un prduit
    [IN : $connBD] : La connexion vers la BD
    [IN : $id]     : L'id du produit
    [IN : $vote]   : Valeur du vote
    [OUT]          : True si réussit, false sinon
    ---------------*/
    function voteProduit($connBD, $id, $vote){
        try {
            $req = $connBD->prepare("UPDATE produit SET Somme = Somme + '$vote', NbVotes = NbVotes+1 WHERE IdProduit = '$id'");
            $reussite = $req->execute();
        } catch (PDOException $e) {
            exit("<p>Erreur lors de l'exécution de la requête SQL :</p>\n" . $e->getMessage());
        }
        $req = NULL;
        return $reussite;
    }

    /*---------------
    [DESC]         : Ajoute au panier le produit sélectionner
    [IN : $id]     : L'id du produit
    [IN : $nom]     : Le nom du produit
    [IN : $prix]     : Le prix du produit
    ---------------*/
    function ajouterAuPanier($id, $nom, $prix, $qte = 1){
        $data = array(
            'id' => $id,
            'nom' => $nom,
            'prix' => $prix,
            'quantite' => $qte
        );
        if (isset($_SESSION['shopping_cart'])) {
            $_SESSION['shopping_cart'][$id] = $data;
        } else {
            $_SESSION['shopping_cart'] = array();
            $_SESSION['shopping_cart'][$id] = $data;
        }
    }

    /*---------------
    [DESC]         : Supprimer panier le produit sélectionner
    [IN : $id]     : L'id du produit
    ---------------*/
    function supprimerDuPanier($idSupp){
        if (isset($_SESSION['shopping_cart'])) {
            unset($_SESSION['shopping_cart'][$idSupp]);
            header('Location: ../pages/panier.php');
        }
    }
    
    /*---------------
    [DESC]         : Vider le panier
    ---------------*/
    function viderPanier(){
        if (isset($_SESSION['shopping_cart'])) {
            $_SESSION['shopping_cart'] = array();
        }
    }

    /*---------------
    [DESC]         : commande le panier d'acaht avec les quantitées demandées
    [IN : $connBD]     : connexion à la bd
    ---------------*/
    function commanderPanier($connBD) {
        foreach($_SESSION['shopping_cart'] as $produit) {
            $date = new DateTime();
            $date = date_format($date, "Y-m-d H:i:s");
            $id = $produit['id'];
            $quantite = $produit['quantite'];
            $quantiteRestante = getQuantiteRestante($connBD, $id);
            if ($quantiteRestante[0] >= $quantite) {
                soustraireQuantite($connBD, $id, $quantite);
                ajouterAchat($connBD, $date, $quantite, $_SESSION['id'], $id);
                supprimerDuPanier($id);
                header('Location: ../pages/panier.php');
            } else {
                $_SESSION['erreurQuantite'][$id] = "En stock: $quantiteRestante[0]";
                header('Location: ../pages/panier.php');
            }
        }
    }

    function console_log( $data ){
        echo '<script>';
        echo 'console.log('. json_encode( $data ) .')';
        echo '</script>';
    }
?>