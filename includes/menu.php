<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <script type="text/javascript" src="../js/recherche.js" defer="defer" ></script>
    <script type="text/javascript" src="../js/utils-ajax.js" defer="defer" ></script>
    <script type="text/javascript" src="../js/divPanier.js" defer="defer"></script>
</head>

<nav>
    <a href="../index.php" class = "lien-logo">Lien du logo</a>
    <div class="barre-recherche">
        <form action="../pages/recherche.php" id="form=recherche" method="get" autocomplete="off">
                <input name="recherche" id="recherche" type="text" placeholder="Rechercher un produit..." list="autocomplete"/>
                <datalist id="autocomplete"></datalist>
                <button type="submit">
                    <span class="material-icons">search</span>
                </button>
        </form>
    </div>
</nav>
<div class = "menu">
    <a href="../index.php">Accueil</a>
    <a href="../pages/mes-achats.php">Mes achats</a>
    <a href="../pages/recherche.php">Catalogue</a>
    <?php 
        if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
           ?> <a href="../pages/deconnexion.php" class="icone"><span class="material-icons">power_settings_new</span></a> 
        <?php } else {
           ?> <a href="../pages/login.php" class="icone"><span class="material-icons">person</span></a>
        <?php }
    ?>
    <a href="../pages/panier.php" id="lienPanier" class="icone"><span class="material-icons">shopping_cart</span></a>
    
    <?php 
        if (isset($_SESSION['estAdmin']) && $_SESSION['estAdmin'] == true) {
    ?>
            <a href="../pages/admin-ajout.php" class="icone"><span class="material-icons">admin_panel_settings</span></a>
        <?php
        }
    ?>
</div>

<div id="divisionPanier" class="divisionPanier">
</div>