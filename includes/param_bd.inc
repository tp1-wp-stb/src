<?php
/**
 * Paramètres de connexion à la BD
 */
define("DB_HOTE", "localhost");
define("DB_NOM", "tp1_magasin");

define("DB_UTIL", "root");
define("DB_MDP", "");

/**
 * Pour créer une connexion à la BD
 *
 * @return \PDO La connexion
 */
function createConnexion()
{
    $connBD = new \PDO(
        "mysql:host=" . DB_HOTE . "; dbname=" . DB_NOM,
        DB_UTIL,
        DB_MDP,
        array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
    );

    // Pour lancer les exceptions lorsqu'il y des erreurs PDO.
    $connBD->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

    return $connBD;
}