<?php 
    $nom="";
    if (isset($_SESSION['nom'])) {
        $nom = $_SESSION['nom'];
    } else if(isset($_SESSION['courriel'])) {
        $nom = $_SESSION['courriel'];
    }
    
?>
<footer>
    <span><?php echo $nom ?></span><!--Place holder user logger-->
    <a href="#" class="icone"><span class="material-icons">email</span></a>
    <a href="#" class="icone"><span class="material-icons">contact_support</span></a>
    <a href="#" class="icone"><span class="material-icons">location_on</span></a>
</footer>