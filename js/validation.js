/**
 * Code Javascript pour la validation de création de compte
 */

"use strict";

var validation = {
    "email" : null,
    "psw" : null,
    "psw2" : null,
    "nom" : null,
    "adresse" : null,
    "errEmail" : null,
    "errPsw" : null,
    "errPsw2" : null,
    "errNom" : null,
    "errAdresse" : null,
    "clientHttp" : null,
    "soumettre": null
}

var regexCourriel = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
var regexNom = /^([a-zA-Z-\s]{3,50})$/;

function gererCourriel(){
    if(validation.errEmail.lastChild != null){
        validation.errEmail.removeChild(validation.errEmail.lastChild);
    }
    if (validation.clientHttp.status !== 200) {
        window.console.error(
            "Erreur (code=" + validation.clientHttp.status +
            "): La requête HTTP n\'a pu être complétée." + validation.clientHttp.url);
    } else {
        var reponse = JSON.parse(validation.clientHttp.responseText);
        if(reponse.disponible == "false"){
            var noeudTexte = document.createTextNode("Le courriel est déjà enregistré");
            validation.errEmail.appendChild(noeudTexte);
        }
    }
}

/**
 * Validation du email
 */
function validEmail(){
    if(validation.errEmail.lastChild != null){
        validation.errEmail.removeChild(validation.errEmail.lastChild);
    }
    validation.email.value = validation.email.value.trim();
    if(!regexCourriel.test(validation.email.value)){
        var noeudTexte = document.createTextNode("Le courriel est invalide");
        validation.errEmail.appendChild(noeudTexte);
    }else{
        //ajax
        var clientHttp = new XMLHttpRequest();
        validation.clientHttp = clientHttp;
        var parametres = {
            "courriel": validation.email.value
        };
        envoyerRequeteAjax(
            validation.clientHttp,
            "../js/ajax/valideEmail.php",
            "POST",
            parametres,
            gererCourriel);
    }
}

/**
 * Validation du mot de passe
 */
function validMDP(){
    if(validation.errPsw.lastChild != null){
        validation.errPsw.removeChild(validation.errPsw.lastChild);
    }
    validation.psw.value = validation.psw.value.trim();
    if(validation.psw.value.length < 4)
    {
        var noeudTexte = document.createTextNode("Doit avoir 4 caractères minimum");
        validation.errPsw.appendChild(noeudTexte);
    }
}

/**
 * Validation de la confirmation du mot de passe
 */
function validConfirmMDP(){
    if(validation.errPsw2.lastChild != null){
        validation.errPsw2.removeChild(validation.errPsw2.lastChild);
    }
    validation.psw2.value = validation.psw2.value.trim();
    if(validation.psw.value != validation.psw2.value)
    {
        var noeudTexte = document.createTextNode("Les mots de passes doivent être identique");
        validation.errPsw2.appendChild(noeudTexte);
    }
}

/**
 * Validation du nom
 */
function validNom(){
    if(validation.errNom.lastChild != null){
        validation.errNom.removeChild(validation.errNom.lastChild);
    }
    validation.nom.value = validation.nom.value.trim();
    if(!regexNom.test(validation.nom.value)){
        var noeudTexte = document.createTextNode("Nom invalide");
        validation.errNom.appendChild(noeudTexte);
    }
}

/**
 * Validation de l'adresse
 */
function validAdresse(){
    if(validation.errAdresse.lastChild != null){
        validation.errAdresse.removeChild(validation.errAdresse.lastChild);
    }
    validation.adresse.value = validation.adresse.value.trim();
    if(validation.adresse.value.length < 3 || validation.adresse.value.length > 200){
        var noeudTexte = document.createTextNode("Adresse invalide");
        validation.errAdresse.appendChild(noeudTexte);
    }
}

function soumettreClick(e){
    if( validation.errEmail.lastChild != null ||
        validation.errPsw.lastChild != null ||
        validation.errPsw2.lastChild != null ||
        validation.errNom.lastChild != null ||
        validation.errAdresse.lastChild != null
    ){
        e.preventDefault();
    }
}

/**
 * Initialisation des événements.
 */
function initialisation()
{
    var email = document.getElementById("email");
    validation.email = email;
    validation.email.addEventListener('focusout', validEmail);

    var psw = document.getElementById("psw");
    validation.psw = psw;
    validation.psw.addEventListener('focusout', validMDP);

    var psw2 = document.getElementById("psw2");
    validation.psw2 = psw2;
    validation.psw2.addEventListener('focusout', validConfirmMDP);

    var nom = document.getElementById("nom");
    validation.nom = nom;
    validation.nom.addEventListener('focusout', validNom);

    var adresse = document.getElementById("adresse");
    validation.adresse = adresse;
    validation.adresse.addEventListener('focusout', validAdresse);


    var errEmail = document.getElementById("errEmail");
    validation.errEmail = errEmail;

    var errPsw = document.getElementById("errPsw");
    validation.errPsw = errPsw;

    var errPsw2 = document.getElementById("errPsw2");
    validation.errPsw2 = errPsw2;

    var errNom = document.getElementById("errNom");
    validation.errNom = errNom;

    var errAdresse = document.getElementById("errAdresse");
    validation.errAdresse = errAdresse;

    var soumettre = document.getElementById("soumettre");
    validation.soumettre = soumettre;
    validation.soumettre.addEventListener('click', soumettreClick);
}

window.addEventListener('load',initialisation ,false);