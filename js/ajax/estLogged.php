<?php

/**
 * Requete post ajax qui vérifie la disponibilité d'un courriel.
 */
session_start();
require_once "utils-ajax.php";
ecrireEnteteJson();

$reponse = array();

if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true){
    http_response_code(200);
    $reponse = array("logged"=>"true");
}else{
    http_response_code(401);
    $reponse = array("logged"=>"false");
}

echo json_encode($reponse, JSON_PRETTY_PRINT);