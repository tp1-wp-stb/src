<?php
/**
 * Created by PhpStorm.
 * User: nrichard
 * Date: 2017-03-15
 * Time: 18:29
 */

/**
 * Écrit le début d'une réponse JSON dans l'entête de la réponse HTTP
 */
function ecrireEnteteJson()
{
    // Retourne du contenu en format JSON.
    header("Content-type: application/json; charset=utf-8");

    // Force l'expiration immédiate de la page au niveau du navigateur Web; elle n'est pas conservée en cache.
    header("Expires: Thu, 19 Nov 1981 08:52:00 GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
    header("Pragma: no-cache");
}


/**
 * Écrit une erreur dans un tableau associatif pour JSON
 * @param $msgErreur string Le message d'erreur
 * @param $codeHttp int Le code de status HTTP (https://fr.wikipedia.org/wiki/Liste_des_codes_HTTP)
 * @return array Tableau avec le message d'erreur
 */
function declarerErreur(string $msgErreur, int $codeHttp)
{
    http_response_code($codeHttp);
    return 
        array(
            "message" => $msgErreur
    );
}
