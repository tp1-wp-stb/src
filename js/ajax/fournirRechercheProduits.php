<?php

/**
 * Requete GET ajax pour avoir les 10 nouveaux produits a afficher dans la page de recherche
 */
require_once "utils-ajax.php";
ecrireEnteteJson();

$reponse = array();

require('../../includes/utilitaire.php');
require('../../includes/param_bd.inc');
try {
    $connBD = createConnexion();
    $recherche = $_GET["recherche"];
    $offset = $_GET["offset"];
    $min = $_GET["min"];
    $max = $_GET["max"];
    $reponse = rechercheProduit($connBD, $recherche, $offset, $min, $max);
    http_response_code(200);
}catch (Exception $e) {
    $reponse = declarerErreur("Erreur lors de la recherche : " . $e->getMessage(), 500);
}


echo json_encode($reponse, JSON_PRETTY_PRINT);