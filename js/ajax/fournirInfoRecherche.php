<?php

/**
 * retourne les infos pour rechercher un produit
 */
session_start();
require_once "utils-ajax.php";
ecrireEnteteJson();

$reponse = array();
if (isset($_SESSION['infoRequete'])){
    $reponse = $_SESSION['infoRequete'];
}

echo json_encode($reponse, JSON_PRETTY_PRINT);