<?php

/**
 * Requete pour avoir le id du produit en fonction du nom
 */
    require('../../includes/param_bd.inc');
    require('../../includes/utilitaire.php');
    
    require_once "utils-ajax.php";
    ecrireEnteteJson();
    
    $response= array();

    try {
        $connBD = createConnexion();
        $input = $_GET["input"];
        $requete = "SELECT * FROM produit WHERE produit.Titre = '$input'";
        $requete.=" LIMIT 0, 1";
        $req =$connBD->prepare($requete);
        
        $req ->execute();
        
        while ($ligneBd = $req->fetch()) {
            $data = array(
                "titre" => $ligneBd['Titre'],
                "idProduit" => $ligneBd['IdProduit']
            );
    
            array_push($response, $data);
        }
             
        $req->closeCursor();
        $connBD = null;   
    } catch (Exception $e) {
        $reponse = declarerErreur("Erreur lors de la recherche : " . $e->getMessage(), 500);
    }

    echo json_encode($response, JSON_PRETTY_PRINT);
?>