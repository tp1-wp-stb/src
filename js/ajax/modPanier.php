<?php

/**
 * Requete post ajax qui vérifie la disponibilité d'un courriel.
 */
session_start();
require_once "utils-ajax.php";
ecrireEnteteJson();

$idMod = $_POST["id"];
$qte = $_POST["qte"];

$_SESSION['shopping_cart'][$idMod]['quantite'] = $qte;

$reponse = array();
if (isset($_SESSION['shopping_cart']) && sizeof($_SESSION['shopping_cart']) > 0){
    $reponse = $_SESSION['shopping_cart'];
}

echo json_encode($reponse, JSON_PRETTY_PRINT);