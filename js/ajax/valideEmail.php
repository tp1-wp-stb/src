<?php

/**
 * Requete post ajax qui vérifie la disponibilité d'un courriel.
 */
require_once "utils-ajax.php";
ecrireEnteteJson();

$reponse = array();

if (empty($_POST["courriel"])) {
    $reponse = declarerErreur('Le paramètre POST "mots-cles" n\'a pas été fourni avec la requête', 400);
} else {
    require('../../includes/utilitaire.php');
    require('../../includes/param_bd.inc');
    try {
        $connBD = createConnexion();
        $reponse = estCourrielDispo($connBD, $_POST["courriel"]) ? array("disponible"=>"true") : array("disponible"=>"false");
        http_response_code(200);
    }catch (Exception $e) {
        $reponse = declarerErreur("Erreur lors de la recherche : " . $e->getMessage(), 500);
    }
}

echo json_encode($reponse, JSON_PRETTY_PRINT);