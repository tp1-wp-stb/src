<?php

/**
 * Requete post ajax qui vérifie la disponibilité d'un courriel.
 */
session_start();
require_once "utils-ajax.php";
ecrireEnteteJson();

$reponse = array();
if (isset($_SESSION['shopping_cart']) &&  sizeof($_SESSION['shopping_cart']) > 0){
    $reponse = $_SESSION['shopping_cart'];
}

echo json_encode($reponse, JSON_PRETTY_PRINT);