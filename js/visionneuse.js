/**
 * Code Javascript pour la visionneuse de la page d'acceuil
 */

"use strict";
var vision ={
    "lien" : null,
    "indice" : 0,
    "minuterie" : null,
    "visionneuse" : null,
    "btnSuivant" : null,
    "btnPrecedent" : null
}

/**
 * Affiche l'item en cours
 */
function rafraichirItem() {
    for (let index = 0; index < 5; index++) {
        vision.lien = document.getElementById("visionneur" + index);
        vision.lien.style.display = "none";
    }
    vision.lien = document.getElementById("visionneur" + vision.indice);
    vision.lien.style.display = "";
}

/**
 * Passe à l'item suivant
 */
function changerPhoto(){
    vision.indice = (vision.indice + 1) % 5;
    rafraichirItem();
}

/**
 * Recule à la photo précédente
 */
function clicPrecedent() {
    vision.indice = (vision.indice + 5 - 1) % 5;
    rafraichirItem();
}

/**
 * Démarre ou arrête la visionneuse
 */
function demarrer()
{
    if (vision.minuterie === null) {
        vision.minuterie = setInterval(changerPhoto, 3000);
    } else {
        clearInterval(vision.minuterie);
        vision.minuterie = null;
    }
}

/**
 * Initialisation des événements.
 */
function initialisation()
{
    rafraichirItem();
    demarrer();
    vision.visionneuse = document.getElementById("visionneuse");
    vision.visionneuse.addEventListener('mouseover', demarrer, false);
    vision.visionneuse.addEventListener('mouseout', demarrer, false);

    var textePrecedent = document.createTextNode("Precedent");
    var precedent = document.createElement("button");
    vision.btnPrecedent = precedent;
    vision.btnPrecedent.appendChild(textePrecedent);
    
    var texteSuivant = document.createTextNode("Suivant");
    var suivant = document.createElement("button");
    vision.btnSuivant = suivant;
    vision.btnSuivant.appendChild(texteSuivant);

    var acceuil = document.getElementById("acceuil1");
    acceuil.appendChild(vision.btnPrecedent);
    acceuil.appendChild(vision.btnSuivant);
    acceuil.style.display = "block";

    vision.btnPrecedent.addEventListener('click', clicPrecedent, false);
    vision.btnSuivant.addEventListener('click', changerPhoto, false);
}

window.addEventListener('load',initialisation ,false);