/**
 * Code Javascript pour la recherche des produits
 */

"use strict";

var scRechercheProduit ={
    "txtRecherche" : null,
    "liste" : null,
    "clientHttp"   : null
};

function gererRetourRechercheId() {
    if (scRechercheProduit.clientHttp.status === 200) {
        try {
            var reponse = JSON.parse(scRechercheProduit.clientHttp.responseText);
            if (reponse.length > 0) {
                document.location.href = "../pages/produit.php?idProduit=" + reponse[0].idProduit;
            } else {
                return ;
            }
        } catch (e) {
            window.console.error(
                "La réponse AJAX n\'est pas une expression JSON valide.");
            window.console.error(e.message);
            window.console.error(scRechercheProduit.clientHttp.responseText);
        }        
    } else {
        window.console.error(
            scRechercheProduit.clientHttp.status + 
            " : " + scRechercheProduit.clientHttp.responseText);
    }
}

function rechercherIdProduit(e) {
    window.console.log(e.target.value);
    
    var clientHttp = new XMLHttpRequest();
    scRechercheProduit.clientHttp = clientHttp;

    var parametres = {
        "input" : e.target.value
    };

    envoyerRequeteAjax(
        scRechercheProduit.clientHttp,
        "../js/ajax/rechProdParNom.php",
        "GET",
        parametres,
        gererRetourRechercheId
    );
}

function gererRetourRecherche() {
    if (scRechercheProduit.clientHttp.status === 200) {
        try {
            window.console.log(scRechercheProduit.clientHttp.responseText);
            var reponse = JSON.parse(scRechercheProduit.clientHttp.responseText);
            window.console.log(reponse);
            
            reponse.forEach(function(item) {
                var option = document.createElement("option");
                option.value = item.titre;
                scRechercheProduit.liste.appendChild(option);
            });
            scRechercheProduit.txtRecherche.addEventListener("change", rechercherIdProduit ,false);
        } catch (e) {
            window.console.error(
                "La réponse AJAX n\'est pas une expression JSON valide.");
            window.console.error(e.message);
            window.console.error(scRechercheProduit.clientHttp.responseText);
        }        
    } else {
        window.console.error(
            scRechercheProduit.clientHttp.status + 
            " : " + scRechercheProduit.clientHttp.responseText);
    }
}

function rechercherProduit() {
    var aChercher = scRechercheProduit.txtRecherche.value.trim();
    window.console.log(aChercher);
    window.console.log(scRechercheProduit.liste.length);
    if (aChercher.length < 3) {
        while (scRechercheProduit.liste.firstChild) {
            scRechercheProduit.liste.removeChild(scRechercheProduit.liste.firstChild);
        }
        return;
    }

    if (scRechercheProduit.clientHttp != null) {
        scRechercheProduit.clientHttp.abort();
    }

    while (scRechercheProduit.liste.firstChild) {
        scRechercheProduit.liste.removeChild(scRechercheProduit.liste.firstChild);
    }
    window.console.log(scRechercheProduit.liste);
    

    var clientHttp = new XMLHttpRequest();
    scRechercheProduit.clientHttp = clientHttp;

    var parametres = {
        "input" : aChercher
    };

    envoyerRequeteAjax(
        scRechercheProduit.clientHttp,
        "../js/ajax/rechercherProduit.php",
        "GET",
        parametres,
        gererRetourRecherche
    );
}



function initialisation() {
    scRechercheProduit.txtRecherche = document.getElementById("recherche");
    scRechercheProduit.liste = document.getElementById("autocomplete");
    scRechercheProduit.txtRecherche.addEventListener("input", rechercherProduit, false);
}

window.addEventListener("load", initialisation, false);
