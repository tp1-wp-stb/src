/**
 * Code Javascript pour la consultation de produit
 */

"use strict";

function ajouterProduit(idProduit, nomProduit)
{
    var produit = {
        "id"      : idProduit,
        "nom"      : nomProduit
    };

    var storage = localStorage.getItem("consultation");
    
    if(storage == ""){
        var tab = [];
    }else{
        var tab = JSON.parse(storage);
    }

    tab = tab.filter(function(value, index, arr){ 
        return value.id != produit.id;
    });
 
    tab.unshift(produit);
    if(tab.length > 5){
        tab.pop();
    }

    localStorage.setItem("consultation", JSON.stringify(tab));
}

/**
 * Initialisation des événements.
 */
function initialisation()
{   
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);

    if(urlParams.has('idProduit')){
        const id = urlParams.get('idProduit');
        const nom = document.getElementById("nomProduit").value;
        ajouterProduit(id, nom);
    }

}

window.addEventListener('load',initialisation ,false);