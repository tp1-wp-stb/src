/**
 * Code Javascript pour la division panier
 */

"use strict";

var panier = {
    "division" : null,
    "lienPanier": null,
    "clientHttp" : null
}

/**
 * Fait apparaitre/disparaitre la division panier
 * @param {element} e
 */
function lienPanier_click(e)
{
    if(panier.lienPanier.contains(e.target) || panier.division.contains(e.target)){
        panier.division.style.display = "block";
    }
    else{
        panier.division.style.display = "none";
    }
}

function checkImage(imageSrc) {
    var img = new Image();
    img.src = imageSrc;

    if(img.width != 0){
        return true;
    }else{
        return false;
    }
}

function rien(){
    window.console.log(JSON.parse(panier.clientHttp.responseText));
}

function modQte(e){
    //ajax update qte session
    var clientHttp = new XMLHttpRequest();
    panier.clientHttp = clientHttp;
    var parametres = {"id":e.target.id, "qte":e.target.value};
    envoyerRequeteAjax(
        panier.clientHttp,
        "../js/ajax/modPanier.php",
        "POST",
        parametres,
        rien);
}

function delItemCart(e){
    //ajax del item session
    var clientHttp = new XMLHttpRequest();
    panier.clientHttp = clientHttp;
    var parametres = {"supprimer":e.target.value};
    envoyerRequeteAjax(
        panier.clientHttp,
        "../js/ajax/delPanier.php",
        "POST",
        parametres,
        gererPanier);
}

/**
 * Création du panier
 */
function gererPanier(){
    while (panier.division.firstChild) {
        panier.division.removeChild(panier.division.firstChild);
    }
    var rep = JSON.parse(panier.clientHttp.responseText);
    if(Object.keys(rep).length == 0){
        var noeudTexte = document.createTextNode("Le panier est vide");
        var p = document.createElement("p");
        p.appendChild(noeudTexte);
        panier.division.appendChild(p);
    }else{
        var table = document.createElement("table");
        table.style.width = "100%";
        for(var item in rep)
        {
            var tr = document.createElement("tr");
            
            var tdIma = document.createElement("td");
            tdIma.rowSpan = "2";
            var ima = document.createElement("img");
            var path = checkImage("/images/produits/" + rep[item].id + ".png")? "/images/produits/" + rep[item].id + ".png" : "/images/produits/image-non-disponible.png";
            ima.setAttribute("src", path);
            ima.setAttribute("width", "125");
            ima.setAttribute("height", "160");
            ima.setAttribute("alt", rep[item].nom);
            tdIma.appendChild(ima);
            tr.appendChild(tdIma);
            
            var tdNom = document.createElement("td");
            var n = document.createTextNode(rep[item].nom);
            var p = document.createElement("p");
            p.appendChild(n);
            tdNom.appendChild(p);
            tr.appendChild(tdNom);

            var tdBtn = document.createElement("td");
            tdBtn.rowSpan = "2";
            var s = document.createElement("span");
            s.className = "material-icons";
            s.value = rep[item].id;
            s.style.cursor = "pointer";
            s.addEventListener('click', delItemCart);
            var n2 = document.createTextNode("close");
            s.appendChild(n2);
            tdBtn.appendChild(s);
            tr.appendChild(tdBtn);

            table.appendChild(tr);
            
            var tr2 = document.createElement("tr");
            var tdIn = document.createElement("td");
            var inp = document.createElement("input");
            inp.type = "number";
            inp.id = rep[item].id;
            inp.min = 1;
            inp.value = rep[item].quantite;
            inp.addEventListener('change', modQte);
            tdIn.appendChild(inp);
            tr2.appendChild(tdIn);

            table.appendChild(tr2);
        }
        var tr = document.createElement("tr");
        var td = document.createElement("td");
        td.colSpan = "3"
        td.style.textAlign = "center";
        var a = document.createElement("a");
        a.href = "/pages/panier.php";
        var n = document.createTextNode("Voir le panier");
        a.appendChild(n);
        td.appendChild(a);
        tr.appendChild(td);
        table.appendChild(tr);

        panier.division.appendChild(table);
    }
}

function gererLogged(){
    if (panier.clientHttp.status !== 200) {
        window.console.error(
            "Erreur (code=" + panier.clientHttp.status +
            "): La requête HTTP n\'a pu être complétée." + panier.clientHttp.url);
    } else {
        var reponse = JSON.parse(panier.clientHttp.responseText);
        if(reponse.logged == "true"){
            document.addEventListener('click', lienPanier_click, false);
            lienPanier.removeAttribute('href');

            //requete ajax pour récupérer le contenu du panier
            var clientHttp = new XMLHttpRequest();
            panier.clientHttp = clientHttp;
            var parametres = {};
            envoyerRequeteAjax(
                panier.clientHttp,
                "../js/ajax/fournirPanier.php",
                "POST",
                parametres,
                gererPanier);
        }
    }
}

/**
 * Initialisation des événements.
 */
function initialisation()
{
    var lienPanier = document.getElementById('lienPanier');
    panier.lienPanier = lienPanier;
    
    var div = document.getElementById('divisionPanier');
    panier.division = div;

    //requete ajax pour savoir si connecté
    var clientHttp = new XMLHttpRequest();
    panier.clientHttp = clientHttp;
    var parametres = {};
    envoyerRequeteAjax(
        panier.clientHttp,
        "../js/ajax/estLogged.php",
        "POST",
        parametres,
        gererLogged);
}

window.addEventListener('load',initialisation ,false);