"use strict";

var maPage = {
    "clientHttp" : null,
    "offset" : null,
    "recherche" : null,
    "min" : null,
    "max" : null,
    "divResultats" : null
}

function gererRetourProduits() {
    window.console.log(maPage.clientHttp);
    if (maPage.clientHttp.status === 200) {
        try {
            window.console.log(maPage.clientHttp.responseText);
            var reponse = JSON.parse(maPage.clientHttp.responseText);
            window.console.log(reponse);

            reponse.forEach(function(item) {
                var aHref = document.createElement("a");
                aHref.setAttribute("href", "produit.php?idProduit="+ item.IdProduit);
                aHref.setAttribute("class", "liens-produits");

                var div = document.createElement("DIV");
                aHref.appendChild(div);

                var img = document.createElement("img");
                img.setAttribute("src", "/images/produits/image-non-disponible.png");
                img.setAttribute("width", "250");
                img.setAttribute("height", "320");
                img.setAttribute("alt", "<?php echo stripslashes(" + item.Titre + "); ?>");
                div.appendChild(img);

                var h2 = document.createElement("h2");
                var titre = document.createTextNode(item.Titre);
                h2.appendChild(titre);
                div.appendChild(h2);

                var span = document.createElement("span");
                var prix = document.createTextNode(item.Prix);
                span.appendChild(prix);
                div.appendChild(span);

                maPage.divResultats.appendChild(aHref);

            });
            
        } catch (e) {
            window.console.error(
                "La réponse AJAX n\'est pas une expression JSON valide.");
            window.console.error(e.message);
            window.console.error(maPage.clientHttp.responseText);
        }        
    } else {
        window.console.error(
            maPage.clientHttp.status + 
            " : " + maPage.clientHttp.responseText);
    }
}

function gererRetourInfo() {
    if (maPage.clientHttp.status === 200) {
        try {
            window.console.log(maPage.clientHttp.responseText);
            var reponse = JSON.parse(maPage.clientHttp.responseText);
            window.console.log(reponse);
            var clientHttp = new XMLHttpRequest();
            maPage.clientHttp = clientHttp;
            maPage.recherche = reponse.recherche;
            maPage.offset = reponse.offset;
            maPage.min = reponse.min;
            maPage.max = reponse.max;

            var parametres = {
                "recherche" : maPage.recherche,
                "offset" : maPage.offset,
                "min" : maPage.min,
                "max" : maPage.max
            };
            envoyerRequeteAjax(
                maPage.clientHttp,
                "../js/ajax/fournirRechercheProduits.php",
                "GET",
                parametres,
                gererRetourProduits
            );
        } catch (e) {
            window.console.error(
                "La réponse AJAX n\'est pas une expression JSON valide.");
            window.console.error(e.message);
            window.console.error(maPage.clientHttp.responseText);
        }        
    } else {
        window.console.error(
            maPage.clientHttp.status + 
            " : " + maPage.clientHttp.responseText);
    }
}

function gererDefilement($infoReq) {
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
        window.console.log(maPage.offset);

        if (maPage.offset == null) {
            var clientHttp = new XMLHttpRequest();
            maPage.clientHttp = clientHttp;
            var parametres = {};
           envoyerRequeteAjax(
               maPage.clientHttp,
                "../js/ajax/fournirInfoRecherche.php",
                "GET",
                parametres,
                gererRetourInfo
            );
        } else {
            var clientHttp2 = new XMLHttpRequest();
            maPage.clientHttp = clientHttp2;
            window.console.log(maPage.offset);
            maPage.offset = maPage.offset + 10;
            window.console.log(maPage.offset);
            var parametres2 = {
                "recherche" : maPage.recherche,
                "offset" : maPage.offset,
                "min" : maPage.min,
                "max" : maPage.max
            };
            envoyerRequeteAjax(
                maPage.clientHttp,
                 "../js/ajax/fournirRechercheProduits.php",
                 "GET",
                 parametres2,
                 gererRetourProduits
             );
        }
    }
}

function initialisation() {
    window.addEventListener('scroll', gererDefilement, false);
    maPage.divResultats = document.getElementById("resultatsRecherche");
    gererDefilement();
}

window.addEventListener('load', initialisation, false);