/**
 * Code Javascript pour afficher les produit consulté du plus récent au plus ancien
 */

"use strict";

var consultation = {
    "main" : null
}

function afficherProduits(){
    var storage = JSON.parse(localStorage.getItem("consultation"));
    
    var n = document.createTextNode("Récemment consultés");
    var h1 = document.createElement("h1");
    h1.appendChild(n);
    consultation.main.appendChild(h1);
    
    var section = document.createElement("section");
    section.className = "accueil";
    consultation.main.appendChild(section);
    
    for(var i = 0; i < storage.length; i++){
        var a = document.createElement("a");
        a.href = "/pages/produit.php?idProduit=" + storage[i].id;
        a.className = "liens-produits";
        section.appendChild(a);
        
        var div = document.createElement("div");
        a.appendChild(div);
        
        var img = document.createElement("img");
        var path = checkImage("/images/produits/" + storage[i].id + ".png")? "/images/produits/" + storage[i].id + ".png" : "/images/produits/image-non-disponible.png";
        img.setAttribute("src", path);
        img.setAttribute("width", "250");
        img.setAttribute("height", "320");
        img.setAttribute("alt", storage[i].nom);
        img.setAttribute( "onerror", "this.src='/images/produits/image-non-disponible.png'");
        div.appendChild(img);

        var n2 = document.createTextNode(storage[i].nom);
        var h2 = document.createElement("h2");
        h2.appendChild(n2);
        div.appendChild(h2);
    }
    
    
}

/**
 * Initialisation des événements.
 */
function initialisation()
{   
    if(localStorage.getItem("consultation") != ""){
        consultation.main = document.getElementsByTagName("main")[0];
        afficherProduits();
    }

}

window.addEventListener('load',initialisation ,false);