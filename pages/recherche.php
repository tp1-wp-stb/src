<!DOCTYPE html>
<?php
    session_start();
    require('../includes/utilitaire.php');
    if (isset($_GET['recherche'])){
        $recherche = testInput($_GET['recherche']);
    }
    else{
        $recherche = "";
    }

    if (isset($_GET['page'])){
        $page = testInput($_GET['page']);
    }
    else{
        $page = 1;
    }

    if (isset($_GET['min'])){
        $min = (is_numeric($_GET['min']))? testInput($_GET['min']) : "";
    }
    else{
        $min = "";
    }

    if (isset($_GET['max'])){
        $max = (is_numeric($_GET['max']))? testInput($_GET['max']) : "";
    }
    else{
        $max = "";
    }

    $page = ($page<1)? 1 : $page;

    if (!empty($recherche)) {
        require('../includes/param_bd.inc');
        $connBD = createConnexion();

        $nbPage = nbPage($connBD, $recherche, $min, $max);
        $page = ($page > $nbPage)? 1 : $page;

        $offset = 10;

        $_SESSION['infoRequete'] = array(
            "offset" => $offset,
            "recherche" => $recherche,
            "min" => $min,
            "max" => $max
        );
        echo json_encode($array);

        $infoRecherche = rechercheProduit($connBD, $recherche, $offset, $min, $max);
        console_log($infoRecherche);
        $connBD = NULL;
    }
?>

<html lang="fr" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" type="text/css" href="../css/style.css" media="all" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
    <script defer="defer" type="text/javascript" src="../js/utils-ajax.js"></script>
    <script defer="defer" type="text/javascript" src="../js/defilement.js"></script>
    <title>Recherche</title>
</head>

<body>
    <?php 
        include("../includes/menu.php");
        include("../includes/header.php");
    ?>
    <main>
        <h1>Catalogue des produits</h1>
        <div class="achats recherche">
            <section class="barre-recherche">
                <form action="recherche.php" method="get">
                    <input name="recherche" type="text" placeholder="Rechercher un produit..." value="<?php echo stripslashes($recherche); ?>" />
                    <input name="min" type="text" placeholder="Prix minimum..." value="<?php if(isset($min)){echo stripslashes($min);}; ?>" />
                    <input name="max" type="text" placeholder="Prix maximum..." value="<?php if(isset($max)){echo stripslashes($max);}; ?>" />
                    <button type="submit">
                        <span class="material-icons">search</span>
                    </button>
                </form>
            </section>

            <section class="accueil" id="resultatsRecherche">
            <?php
                if(isset($infoRecherche)){
                    if(sizeof($infoRecherche) != 0){
                        foreach($infoRecherche as $item){
                            $offset++;
                            $imgSrc = "/images/produits/".stripslashes($item[0]).".png";
                            if (!file_exists("{$_SERVER['DOCUMENT_ROOT']}" . $imgSrc)) {
                                $imgSrc = "/images/produits/image-non-disponible.png";
                            }
            ?>
                                <a href="produit.php?idProduit=<?php echo stripslashes($item[0]); ?>" class ="liens-produits">
                                    <div>
                                        <img src="<?php echo $imgSrc; ?>" width="250" height="320" alt="<?php echo stripslashes($item[1]); ?>" />
                                        <h2><?php echo stripslashes($item[1]); ?></h2>
                                        <span><?php echo stripslashes($item[3]); ?> $</span>
                                    </div>
                                </a>
                        <?php
                        }
                    }else{
                        ?>
                        <section>Aucun résultat</section>
                    <?php    
                    }
                }   ?>
            </section>
        </div>
    </main>
    <?php 
        include("../includes/pdp.php");
    ?>
</body>

</html>