<!DOCTYPE html>
<?php
/*if not logged et admin, redirection err403*/
session_start();

if (!isset($_SESSION['estAdmin']) && !$_SESSION['estAdmin'] == true){
    header("Location: ../pages/err403.php");
}

require('../includes/param_bd.inc');
require('../includes/utilitaire.php');

$msgOk = "";
$errTitre = $errDesc = $errPrix = $errQte = $errSum = $errVote = $errImage = "";
$titre = $desc = $prix = $qte = $sum = $vote = "";


if ($_SERVER["REQUEST_METHOD"] == "GET"){
    if (!isset($_GET['idProduit']) || empty($_GET['idProduit']) || !is_numeric($_GET['idProduit'])) {
        header('Location: err404.php');
    }

    $id = testInput($_GET['idProduit']);
    $connBD = createConnexion();
    $infoProduit = getProduit($connBD, $id);
    $connBD = NULL;

    if(sizeof($infoProduit) === 0){
        header("Location: ../pages/err404.php");
    }else{
        $titre = $infoProduit[0][1];
        $desc = $infoProduit[0][2];
        $prix = $infoProduit[0][3];
        $qte = $infoProduit[0][4];
        $sum = $infoProduit[0][5];
        $vote = $infoProduit[0][6];
    }
}
/* Si envoie de formulaire, validation des champs */
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id = testInput($_POST['idProduit']);

    $titre = testInput($_POST['titre']);
    if(!estChaine($titre,3,50)){
        $titre = "";
        $errTitre = "Titre invalide";
    }

    $desc = testInput($_POST['desc']);
    if(!estChaine($desc,3,200)){
        $desc = "";
        $errDesc = "Description invalide";
    }

    $prix = testInput($_POST['prix']);
    if(!estChiffre($prix,0.001)){
        $prix = "";
        $errPrix = "Prix invalide";
    }
    
    $qte = testInput($_POST['qte']);
    if(!estChiffre($qte,0)){
        $qte = "";
        $errQte = "Quantité invalide";
    }

    $sum = testInput($_POST['sum']);
    if(!estChiffre($sum,0)){
        $sum = "";
        $errSum = "Somme invalide";
    }

    $vote = testInput($_POST['vote']);
    if(!estChiffre($vote,0)){
        $vote = "";
        $errVote = "Vote invalide";
    }
    
    /* téléversement d'une nouvelle image facultative */
    if($_FILES["image"]["error"] != 4){
        if ($_FILES["image"]['error'] != 0) {
            $errImage = "Erreur lors du chargement de l'image";
        }else{
            $infosfichier = pathinfo($_FILES["image"]['name']);
            $extension = strtolower($infosfichier['extension']);
            if ($extension != 'png'){
                $errImage = "Extension invalide (seulement .png accepté)";
            }
        }
    }

    //Si validation réussit, update du produit et de l'image
    if(!empty($titre) 
        && !empty($desc) 
        && !empty($prix) 
        && (!empty($qte) || $qte == "0") 
        && (!empty($sum) || $sum == "0") 
        && (!empty($vote) || $vote == "0") 
        && empty($errImage))
    {
        $msgOk = "Produit modifié avec succès";
        $connBD = createConnexion();
        modifProduit($connBD, $id, $titre, $desc, $prix, $qte, $sum, $vote);
        $connBD = NULL;

        if($_FILES["image"]["error"] != 4){
            $chemin = "../images/produits/".$id.".png";
            if (file_exists($chemin)) {
                unlink($chemin);
            }
            move_uploaded_file($_FILES["image"]['tmp_name'], $chemin);
        }

        $titre = $desc = $prix = $qte = $sum = $vote = "";

        header("Location: ../pages/produit.php?idProduit=".stripslashes($id));
    }
}
?>
<html lang="fr" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" type="text/css" href="../css/style.css" media="all" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
    <script defer="defer" type="text/javascript" src="../js/utils-ajax.js"></script>
    <title>Admin - Modification</title>
</head>

<body>
    <?php 
        include("../includes/menu.php");
        include("../includes/header.php");
    ?>
    <main>
        <h1>Modification du produit #<?php echo stripslashes($id); ?></h1>
        <span class="erreur" style="color:green;"><?php echo $msgOk ?></span>

        <form action="admin-modification.php" method="post" enctype="multipart/form-data">
            <input type="hidden" name="idProduit" id="idProduit" value="<?php echo stripslashes($id) ?>" />
            <label for="titre">Titre</label>
            <input type="text" id="titre" name="titre" value="<?php echo stripslashes($titre) ?>" placeholder="Entre 3 et 50 charactère..." required /><span class="erreur"><?php echo $errTitre ?></span><br>

            <label for="desc">Description</label>
            <textarea name="desc" id="desc" cols="50" rows="4" placeholder="Entre 3 et 200 charactère..." value="" required><?php echo stripslashes($desc) ?></textarea><span class="erreur"><?php echo $errDesc ?></span><br>

            <label for="prix">Prix</label>
            <input type="text" id="prix" name="prix" value="<?php echo stripslashes($prix) ?>" placeholder="Plus grand que 0..." required /><span class="erreur"><?php echo $errPrix ?></span><br>

            <label for="qte">Quantité</label>
            <input type="text" id="qte" name="qte" value="<?php echo stripslashes($qte) ?>" placeholder="Minimum 0..." required /><span class="erreur"><?php echo $errQte ?></span><br>

            <label for="sum">Somme des votes</label>
            <input type="text" id="sum" name="sum" value="<?php echo stripslashes($sum) ?>" placeholder="Minimum 0..." required /><span class="erreur"><?php echo $errSum ?></span><br>

            <label for="vote">Nombre de vote</label>
            <input type="text" id="vote" name="vote" value="<?php echo stripslashes($vote) ?>" placeholder="Minimum 0..." required /><span class="erreur"><?php echo $errVote ?></span><br>

            <label for="actuel">Image actuelle</label>
            <?php
                $imgSrc = "/images/produits/".stripslashes($id).".png";
                if (!file_exists("{$_SERVER['DOCUMENT_ROOT']}" . $imgSrc)) {
                    $imgSrc = "/images/produits/image-non-disponible.png";
                }
            ?>
            <img src="<?php echo "$imgSrc"; ?>" alt="<?php echo stripslashes($titre); ?>" id="actuel" name="actuel" /><br>

            <label for="image">Nouvelle Image</label>
            <input type="file" id="image" name="image" /><span class="erreur"><?php echo $errImage ?></span><br><br>

            <button type="submit" class="envoie-admin">
                Envoyer <span class="material-icons">send</span>
            </button>
        </form>
    </main>
    <?php 
        include("../includes/pdp.php");
    ?>
</body>

</html>