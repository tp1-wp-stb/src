<!DOCTYPE html>
<?php
    session_start();
    if (!isset($_GET['idProduit']) || empty($_GET['idProduit']) || !is_numeric($_GET['idProduit'])) {
        header('Location: err404.php');
    }

    require('../includes/utilitaire.php');

    $id = testInput($_GET['idProduit']);

    require('../includes/param_bd.inc');
    
    if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true){
        if (isset($_GET['vote']) && !empty($_GET['vote']) && is_numeric($_GET['vote'])) {
            $connBD = createConnexion();
            $aVote = voteProduit($connBD, $id, $_GET['vote']);
            $connBD = NULL;
        }
    }

    
    $connBD = createConnexion();
    $infoProduit = getProduit($connBD, $id);
    $connBD = NULL;
    
    if (sizeof($infoProduit) === 0){
        header('Location: err404.php');
    }
    $info = $infoProduit[0];

    if (isset($_POST["add"])) {
        ajouterAuPanier($info[0], $info[1], $info[3]);
        header('Location: panier.php');
    }
?>


<html lang="fr" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" type="text/css" href="../css/style.css" media="all" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
    <script defer="defer" type="text/javascript" src="../js/utils-ajax.js"></script>
    <script defer="defer" type="text/javascript" src="../js/produitConsulte.js"></script>
    <script defer="defer" type="text/javascript" src="../js/afficherConsultation.js"></script>
    <title>Page des produits</title>
</head>

<body>
    <?php 
        include("../includes/menu.php");
        include("../includes/header.php");
    ?>
    <main>
        <h1>
            <input type="hidden" id="nomProduit" name="nomProduit" value="<?php echo stripslashes($info[1]); ?>">
            <?php 
                echo stripslashes($info[1]); 
                if (isset($_SESSION['estAdmin']) && $_SESSION['estAdmin'] == true) {
            ?>
                    <a href="admin-modification.php?idProduit=<?php echo stripslashes($id); ?>" class="lien-admin"><span class="material-icons">create</span></a> <!-- Lien de modification pour admin -->
                    <?php
                }
            ?>
        </h1>
        <section>

            <?php
                $imgSrc = "/images/produits/".stripslashes($info[0]).".png";
                if (!file_exists("{$_SERVER['DOCUMENT_ROOT']}" . $imgSrc)) {
                    $imgSrc = "/images/produits/image-non-disponible.png";
                }
            ?>

            <img src="<?php echo "$imgSrc"; ?>" alt="<?php echo stripslashes($info[1]); ?>" />
            <table title="tableProduit" class="table-produit">
                <tr>
                    <th>Prix</th>
                    <td><?php echo stripslashes($info[3]); ?> $</td>
                </tr>
                <tr>
                    <th>Quantité disponible</th>
                    <td><?php echo stripslashes($info[4]); ?></td>
                </tr>
                <tr>
                    <th>Note</th>
                    <td>
                        <?php
                            if($info[6] != 0){
                                echo number_format(stripslashes($info[5])/stripslashes($info[6]), 2, '.', '')."/5"; 
                            }else{
                                echo "Aucune note";
                            }
                            
                        ?>
                    </td>
                </tr>
                <?php 
                    if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
                ?>
                        <tr> <!-- Seulement pour les user logger -->
                            <th>Notez ce produit </th>
                            <td>
                            <form action="produit.php" method="get">
                                <input type="range" id="vote" name="vote" min="0" max="5" step="1" />
                                <input type="hidden" id="idProduit" name="idProduit" value="<?php echo stripslashes($id); ?>" />
                                <button type="submit" class="icone-vote">
                                    <span class="material-icons" >thumb_up_alt</span>
                                </button>
                                <?php
                                    if(isset($aVote) && $aVote == true){
                                ?>
                                    <span class="erreur" style="color:green;">Votre vote à été comptabilisé</span>
                                <?php
                                    }
                                ?>
                            </form>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                <?php 
                    if ($info[4] != 0) {
                ?>
                    <tr> <!-- Seulement si nombre de produit dispo != 0-->
                        <th>Ajouter au panier </th>
                        <td>
                        <form method="post">
                            <button type="submit" name="add" class="icone-vote"><span class="material-icons">add_shopping_cart</span></button><!-- Ajoute a la session -->
                        </form>
                        </td>
                    </tr>
                <?php
                }
                ?>

            </table>
        </section>
        <h2>Description</h2>
        <p><?php echo stripslashes($info[2]); ?></p>
    </main>
    <?php
        include("../includes/pdp.php");
    ?>
</body>

</html>