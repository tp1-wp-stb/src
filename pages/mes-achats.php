<!DOCTYPE html>
<?php
    session_start();
    if (!isset($_SESSION["courriel"])) {
        header('Location: ./login.php');
    }

    require('../includes/param_bd.inc');
    require('../includes/utilitaire.php');
    
    $connBD = createConnexion();
    $achats = getAchats($connBD, $_SESSION['id']);
    $connBD = NULL;
?>
<html lang="fr" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/style.css" media="all" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script defer="defer" type="text/javascript" src="../js/utils-ajax.js"></script>
    <title>Mes achats</title>
</head>

<body>
    <?php 
        include("../includes/menu.php");
        include("../includes/header.php");
    ?>
    <main>
        <h1>Mes achats</h1>
        <div class="achats">

            <?php
                if(count($achats)==0){
                    echo "Aucun achat";
                }else{
                    foreach($achats as $achat){
                        ?>
                        <a href="./produit.php?idProduit=<?php echo stripslashes($achat[4]); ?>" class="lien-achats">
                            <section>
                                <article>Date d'achat : <?php echo stripslashes($achat[0]); ?></article>
                                <article>Nom : <?php echo stripslashes($achat[5]); ?></article>
                                <article>Prix : <?php echo stripslashes($achat[7]); ?></article>
                                <article>Qte : <?php echo stripslashes($achat[1]); ?></article>
                            </section>
                        </a>
                        <?php
                    }
                }
            ?>
        </div>
    </main>
    <?php 
        include("../includes/pdp.php");
    ?>
</body>

</html>