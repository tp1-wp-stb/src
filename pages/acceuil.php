<?php
    require('./includes/param_bd.inc');
    require('./includes/utilitaire.php');
    $connBD = createConnexion();
    $infoBest = getBest($connBD);
    $infoStock = getStock($connBD);
    $connBD = NULL;
?>
<script type="text/javascript" src="../js/visionneuse.js" defer="defer"></script>
<script defer="defer" type="text/javascript" src="../js/afficherConsultation.js"></script>
<h1>Visionneuse des plus populaires</h1>
<section id="acceuil1" class="accueil">
    <ol id="visionneuse">
    <?php
    $indice = 0;
    foreach($infoBest as $item){
        $imgSrc = "/images/produits/".stripslashes($item[0]).".png";
        if (!file_exists("{$_SERVER['DOCUMENT_ROOT']}" . $imgSrc)) {
            $imgSrc = "/images/produits/image-non-disponible.png";
        }
    ?>
        <li id="visionneur<?php echo $indice; ?>">
            <a href="pages/produit.php?idProduit=<?php echo stripslashes($item[0]); ?>" class ="liens-produits">
                <div>
                    <img src="<?php echo $imgSrc; ?>" width="250" height="320" alt="<?php echo stripslashes($item[1]); ?>" />
                    <h2><?php echo stripslashes($item[1]); ?></h2>
                    <span><?php echo stripslashes($item[3]); ?> $</span>
                </div>
            </a>
        </li>
    <?php
        $indice++;
    }
    ?>
    </ol>
</section>
<!--
<h1>Les plus populaires</h1>
<section class="accueil">
    <?php
    foreach($infoBest as $item){
        $imgSrc = "/images/produits/".stripslashes($item[0]).".png";
        if (!file_exists("{$_SERVER['DOCUMENT_ROOT']}" . $imgSrc)) {
            $imgSrc = "/images/produits/image-non-disponible.png";
        }
    ?>
        <a href="pages/produit.php?idProduit=<?php echo stripslashes($item[0]); ?>" class ="liens-produits">
            <div>
                <img src="<?php echo $imgSrc; ?>" width="250" height="320" alt="<?php echo stripslashes($item[1]); ?>" />
                <h2><?php echo stripslashes($item[1]); ?></h2>
                <span><?php echo stripslashes($item[3]); ?> $</span>
            </div>
        </a>
    <?php
    }
    ?>
</section>
-->
<h1>En stock</h1>
<section class="accueil">
    <?php
    foreach($infoStock as $item){
        $imgSrc = "/images/produits/".stripslashes($item[0]).".png";
        if (!file_exists("{$_SERVER['DOCUMENT_ROOT']}" . $imgSrc)) {
            $imgSrc = "/images/produits/image-non-disponible.png";
        }
    ?>
        <a href="pages/produit.php?idProduit=<?php echo stripslashes($item[0]); ?>" class ="liens-produits">
            <div>
                <img src="<?php echo $imgSrc; ?>" width="250" height="320" alt="<?php echo "$item[1]"; ?>" />
                <h2><?php echo stripslashes($item[1]); ?></h2>
                <span><?php echo stripslashes($item[3]); ?> $</span>
            </div>
        </a>
    <?php
    }
    ?>
    
</section>
