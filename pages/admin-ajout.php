<?php
/*if not logged et admin, redirection err403*/
session_start();

if (!isset($_SESSION['estAdmin']) && !$_SESSION['estAdmin'] == true){
    header("Location: ../pages/err403.php");
}

require('../includes/param_bd.inc');
require('../includes/utilitaire.php');

$msgOk = "";
$errTitre = $errDesc = $errPrix = $errQte = $errSum = $errVote = $errImage = "";
$titre = $desc = $prix = $qte = $sum = $vote = "";

/* Si envoie de formulaire, validation des champs */
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $titre = testInput($_POST['titre']);
    if(!estChaine($titre,3,50)){
        $titre = "";
        $errTitre = "Titre invalide";
    }

    $desc = testInput($_POST['desc']);
    if(!estChaine($desc,3,200)){
        $desc = "";
        $errDesc = "Description invalide";
    }

    $prix = testInput($_POST['prix']);
    if(!estChiffre($prix,0.001)){
        $prix = "";
        $errPrix = "Prix invalide";
    }
    
    $qte = testInput($_POST['qte']);
    if(!estChiffre($qte,0)){
        $qte = "";
        $errQte = "Quantité invalide";
    }

    $sum = testInput($_POST['sum']);
    if(!estChiffre($sum,0)){
        $sum = "";
        $errSum = "Somme invalide";
    }

    $vote = testInput($_POST['vote']);
    if(!estChiffre($vote,0)){
        $vote = "";
        $errVote = "Vote invalide";
    }

    if ($_FILES["image"]['error'] != 0) {
        $errImage = "Erreur lors du chargement de l'image";
    }else{
        $infosfichier = pathinfo($_FILES["image"]['name']);
        $extension = strtolower($infosfichier['extension']);
        if ($extension != 'png'){
            $errImage = "Extension invalide (seulement .png accepté)";
        }
    }

    //Si validation réussit, création produit et upload de l'image
    if(!empty($titre) 
        && !empty($desc) 
        && !empty($prix) 
        && (!empty($qte) || $qte == "0") 
        && (!empty($sum) || $sum == "0") 
        && (!empty($vote) || $vote == "0") 
        && empty($errImage))
    {
        $connBD = createConnexion();
        $id = ajoutProduit($connBD, $titre, $desc, $prix, $qte, $sum, $vote);
        $connBD = NULL;

        $msgOk = "Produit créé avec succès <a href='produit.php?idProduit=".$id."'>Voir le produit</a>";
        $chemin = "../images/produits/".$id.".png";

        move_uploaded_file($_FILES["image"]['tmp_name'], $chemin);

        $titre = $desc = $prix = $qte = $sum = $vote = "";
    }
}
?>

<!doctype html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/style.css" media="all" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script defer="defer" type="text/javascript" src="../js/utils-ajax.js"></script>
    <title>Admin - AJout</title>
</head>

<body>
    <?php 
        include("../includes/menu.php");
        include("../includes/header.php");
    ?>
    <main>
        <h1>Ajout d'un produit</h1>
        <span class="erreur" style="color:green;"><?php echo $msgOk ?></span>

        <form action="admin-ajout.php" method="post" enctype="multipart/form-data">
            <label for="titre">Titre</label>
            <input type="text" id="titre" name="titre" value="<?php echo stripslashes($titre); ?>" placeholder="Entre 3 et 50 charactère..." required><span class="erreur"><?php echo $errTitre; ?></span><br>

            <label for="desc">Description</label>
            <textarea name="desc" id="desc" cols="50" rows="4" placeholder="Entre 3 et 200 charactère..." required><?php echo stripslashes($desc) ?></textarea><span class="erreur"><?php echo $errDesc; ?></span><br>

            <label for="prix">Prix</label>
            <input type="text" id="prix" name="prix" value="<?php echo stripslashes($prix); ?>" placeholder="Plus grand que 0..." required><span class="erreur"><?php echo $errPrix; ?></span><br>

            <label for="qte">Quantité</label>
            <input type="text" id="qte" name="qte" value="<?php echo stripslashes($qte); ?>" placeholder="Minimum 0..." required><span class="erreur"><?php echo $errQte; ?></span><br>

            <label for="sum">Somme des votes</label>
            <input type="text" id="sum" name="sum" value="<?php echo stripslashes($sum); ?>" placeholder="Minimum 0..." required><span class="erreur"><?php echo $errSum; ?></span><br>

            <label for="vote">Nombre de vote</label>
            <input type="text" id="vote" name="vote" value="<?php echo stripslashes($vote); ?>" placeholder="Minimum 0..." required><span class="erreur"><?php echo $errVote; ?></span><br>

            <label for="image">Image</label>
            <input type="file" id="image" name="image" required><span class="erreur"><?php echo $errImage; ?></span><br><br>

            <button type="submit" class="envoie-admin">
                Envoyer <span class="material-icons">send</span>
            </button>
        </form>
    </main>
    <?php 
        include("../includes/pdp.php");
    ?>
</body>

</html>