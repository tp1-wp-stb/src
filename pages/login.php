<!DOCTYPE html>
<?php
session_start();
$_SESSION['loggedin'] = false;
require('../includes/param_bd.inc');
require('../includes/utilitaire.php');
$connBD = createConnexion();

$email = '';
$nom='';
$flagRememberMe = false;
if (isset($_COOKIE['rememberme'])) {
    $email = $_COOKIE['rememberme'];
    $flagRememberMe = true;
}
$msgError = [];

$context = 'login';
if (isset($_GET['context'])) {
    $context = $_GET['context'];
}

if ($context === 'login') {

    if (isset($_POST) && !empty($_POST)) {

        //Validation Utilisateur
        if (!isset($_POST['email']) || empty($_POST['email']) || !estChaine(testInput($_POST['email'],3,50))) {
            array_push($msgError, "Email invalide.");
        } else {
            $email = $_POST['email'];
        }
        //Validation mot de passe
        if (!isset($_POST['psw']) || empty($_POST['psw']) || !estChaine(testInput($_POST['psw'],3,50))) {
            array_push($msgError, "Mot de passe invalide.");
        }

        if (sizeof($msgError) === 0) {
            //Validation du couple email, mdp
            try {
                $req = $connBD->prepare('SELECT * FROM `utilisateur` WHERE courriel=:email and mdp=:psw');
                $req->execute(array(
                    "email" => $_POST['email'],
                    "psw" => hash('sha512', $_POST['psw'])
                ));
                $result = $req->fetchAll();
                if (sizeof($result) === 0) {
                    //Utilisateur inexistant
                    array_push($msgError, "L'utilisateur est invalide.");
                } else {

                    if (isset($_POST['rememberme']) && $_POST['rememberme'] === 'on') {
                        setcookie("rememberme", $result[0]['Courriel'], time() + 3600);
                    }

                    // Mon utilisateur existe
                    $idUtilisateur = $result[0]['IdUtilisateur'];
                    setcookie("IdUtilisateur", $idUtilisateur, time() + 3600);
                    setcookie("nomUtilisateur", $result[0]['Nom'], time() + 3600);
                    session_start();
                    $_SESSION['nomUtilisateur'] = $result[0]['Nom'];
                    $_SESSION['loggedin'] = true;
                    $_SESSION['courriel'] = $email;
                    $_SESSION['nom'] = $result[0]['Nom'];
                    $_SESSION['estAdmin'] = $result[0]['EstAdministrateur'];
                    $_SESSION['id'] = $result[0][0];
                    $_SESSION['shopping_cart'] = array();
                    header("Location: ../index.php");
                }
            } catch (PDOException $e) {
                exit("<p>Erreur lors de l'exécution de la requête SQL :</p>\n" . $e->getMessage());
            }
        }
    }
}

if ($context === 'creation') {

    if (isset($_POST) && !empty($_POST)) {
        //Mode création
        //Valider email
        if (!isset($_POST['email']) || empty($_POST['email']) || !estChaine(testInput($_POST['email'],3,50))) {
            array_push($msgError, "Email invalide.");
        } else {
            $email = $_POST['email'];
        }
        //Valider nom
        if (!isset($_POST['nom']) || empty($_POST['nom']) || !estChaine(testInput($_POST['nom'],1,50))) {
            array_push($msgError, "Nom invalide.");
        } else {
            $nom = $_POST['nom'];
        }
        //Valider adresse
        if (!isset($_POST['adresse']) || empty($_POST['adresse']) || !estChaine(testInput($_POST['adresse'],1,200))) {
            array_push($msgError, "Adresse invalide.");
        } else {
            $nom = $_POST['adresse'];
        }
        //Valider mot de passe
        if (!isset($_POST['psw']) || empty($_POST['psw']) || !estChaine(testInput($_POST['psw'],1,128))) {
            array_push($msgError, "Mot de passe invalide.");
        }
        if (!isset($_POST['psw2']) || empty($_POST['psw2']) || !estChaine(testInput($_POST['psw2'],1,128))) {
            array_push($msgError, "Mot de passe invalide.");
        }

        if (sizeof($msgError) === 0) {
            //Valider deux mots de passes identique.
            if ($_POST['psw'] !== $_POST['psw2']) {
                array_push($msgError, "Les deux mots de passe ne concordent pas");
            } else {

                try {

                    $req = $connBD->prepare('SELECT * FROM `utilisateur` WHERE courriel=:email');
                    $req->execute(array(
                        "email" => $_POST['email'],
                    ));
                    $result = $req->fetchAll();

                    if (sizeof($result) > 0) {
                        //Utilisateur déjà existant
                        array_push($msgError, "(L'utilisateur existe deja...)");
                    } else {
                        //Insérer les infos dans la bd
                        $req = $connBD->prepare('INSERT INTO utilisateur (Courriel, mdp, Nom, AdressePostale, EstAdministrateur) VALUES (:email, :psw, :nom, :address, false)');
                        $req->execute(array(
                            "email" => $_POST['email'],
                            "psw" => hash('sha512', $_POST['psw']),
                            "nom" => $_POST['nom'],
                            "address" => $_POST['adresse'],
                        ));

                        $idUtilisateur = $connBD->lastInsertId();
                        setcookie("IdUtilisateur", $idUtilisateur, time() + 3600);
                        setcookie("nomUtilisateur", 'NOUVEL UTILISATEUR:)', time() + 3600);
                        session_start();

                        $_SESSION['nomUtilisateur'] = $result[0]['Nom'];
                        $_SESSION['loggedin'] = true;
                        $_SESSION['courriel'] = $email;
                        $_SESSION['nom'] = $result[0]['Nom'];
                        $_SESSION['estAdmin'] = $result[0]['EstAdministrateur'];
                        $_SESSION['id'] = $result[0][0];
                        $_SESSION['shopping_cart'] = array();
                        header("Location: ../index.php");
                    }
                } catch (PDOException $e) {

                    exit("<p>Erreur lors de l'exécution de la requête SQL :</p>\n" . $e->getMessage());
                }
            }
        }
    }
}
?>

<html lang="fr" xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" type="text/css" href="../css/style.css" media="all" />
    <script defer="defer" type="text/javascript" src="../js/utils-ajax.js"></script>
    <script defer="defer" type="text/javascript" src="../js/validation.js"></script>
    <title>Recherche</title>
    </head>
    <body class="login">
        <div class="form">
        <form action="login.php?context=<?php echo ($context) ?>" method="post">
            <?php if ($context === 'login') { ?>
                
                <div class="login-header">
                    <a href="../index.php"><img src="../images/logo.png" alt="logo site" /></a>
                    <h4>Identification</h4>
                </div>

                <div>
                    <label for="email">Adresse courriel: </label>
                    <input type="text" name="email" id="email" value="<?php echo ($email) ?>" />
                </div>
                <div>
                    <label for="psw" >Mot de passe: </label>
                    <input type="password" name="psw" id="psw" value="" />
                </div>

                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="rememberme" name="rememberme" <?php echo ($flagRememberMe) ? 'checked' : '' ?> /> Se souvenir de moi
                    </label>
                </div>

                <?php if (sizeof($msgError)) { ?>
                    <div class="error" role="alert">
                        <?php echo implode("<br>", $msgError) ?>
                    </div>
                <?php } ?>
                
                <button type="submit">Identifier</button>

                <div class="lien">
                    <p>Pas de compte? <a href="login.php?context=creation">Créer un compte!</a></p>
                </div>


            <?php } ?>
            <?php if ($context === 'creation') { ?>

                <div class="login-header">
                    <a href="../index.php"><img src="../images/logo.png" alt="logo site" /></a>
                    <h4>Creation</h4>
                </div>

                <div>
                    <label for="email">Adresse courriel</label>
                    <input type="text" name="email" id="email" value="<?php echo ($email) ?>" />
                    <p id="errEmail" class="error"></p>
                </div>
                <div>
                    <label for="psw">Mot de passe</label>
                    <input type="password" name="psw" id="psw" value="" />
                    <p id="errPsw" class="error"></p>
                </div>
                <div>
                    <label for="psw2">Vérification mot de passe</label>
                    <input type="password" name="psw2" id="psw2" value="" />
                    <p id="errPsw2" class="error"></p>
                </div>
                <div>
                    <label for="nom">Nom</label>
                    <input type="text" name="nom" id="nom" value="" />
                    <p id="errNom" class="error"></p>
                </div>
                <div>
                    <label for="adresse">Adresse</label>
                    <input type="text" name="adresse" id="adresse" value="" />
                    <p id="errAdresse" class="error"></p>
                </div>

                <?php if (sizeof($msgError)) { ?>
                    <div class="error" role="alert">
                        <?php echo implode("<br>", $msgError) ?>
                    </div>
                <?php } ?>
                
                <button id="soumettre" type="submit">Créer un compte</button>

                <div class ="lien">
                    <p>Déjà un compte? <a href="login.php?context=login">Identifiez vous!</a></p>
                </div>

            <?php } ?>
        </form>
        
        </div>
    </body>
</html>