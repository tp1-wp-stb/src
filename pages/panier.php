<!DOCTYPE html>

<?php
    require('../includes/utilitaire.php');
    require('../includes/param_bd.inc');
    session_start();
    $shoppingCart = array();
    $connBD = createConnexion();
    /* if not logged, redirection err403*/
    if (!isset($_SESSION["courriel"])) {
        header('Location: ./login.php');
    }
    if(isset($_POST["vider"])) {
        viderPanier();
    }

    if(isset($_POST["supprimer"])) {
        $id = $_POST['supprimer'];
        supprimerDuPanier($id);
        unset($_POST['supprimer']);
    }
    
    if (isset($_POST['commander'])) {
        foreach($_POST['quantiteProduit'] as $key => $value) {
            $_SESSION['shopping_cart'][$key]['quantite'] = $value;
        }
        commanderPanier($connBD);
    }
    
?>

<html lang="fr" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" type="text/css" href="../css/style.css" media="all" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
    <script defer="defer" type="text/javascript" src="../js/utils-ajax.js"></script>
    <title>Mon panier</title>
</head>

<body>
    <?php 
        include("../includes/menu.php");
        include("../includes/header.php");
        ?>
    <main>
        <h1>Mon panier</h1>
        <form method="post">
            <div class="achats">
                <table class="table-cart"><!-- Template produit -->
                    <?php
                    if (isset($_SESSION['shopping_cart']) &&  sizeof($_SESSION['shopping_cart']) > 0) {
                        ?>
                        <tr style="text-align: left;">
                            <th width="25%">Nom</th>
                            <th width="25%">Quantité</th>
                            <th width="25%">Prix</th>
                            <th width="25%">Total</th>
                        </tr> <?php
                        $total = 0;

                        foreach($_SESSION['shopping_cart'] as $produit) {
                            $shoppingCart[$produit['id']] = $produit;
                        ?>
                        <tr>
                            <td><?php echo $produit['nom']; ?></td>
                            <td><input name="quantiteProduit[<?php echo $produit['id'] ?>]" type="number" min="1" value="<?php echo $produit['quantite']?>"></input></td>
                            <td><?php echo number_format($produit['prix'],2); ?></td>
                            <td><?php echo number_format((int)$produit['quantite'] * (int)$produit['prix'], 2); ?></td>
                            <td>
                            <?php 
                                if(isset($_SESSION['erreurQuantite'][$produit['id']])) { 
                                    echo $_SESSION['erreurQuantite'][$produit['id']];
                                }
                            ?>
                            </td>
                            <td><button type="submit" name="supprimer" value="<?php echo $produit['id'] ?>" class="icon-vote"><span class="material-icons">close</span></button></td>
                        </tr> <?php
                        }
                    } else {
                        echo'
                        <tr>
                            <td>Le panier est vide!</td>
                        </tr>
                        ';
                    }
                    ?>
                </table>
            </div>
            <div class="checkout">
                <button type="submit" name="vider" value="vider">Vider le panier</button>
                <button type="submit" name="commander" value="commander" <?php if(!isset($_SESSION['shopping_cart']) || sizeof($_SESSION['shopping_cart']) == 0){ echo "disabled"; } ?>>Commander</button>
            </div>
        </form>
    </main>
    <?php 
        include("../includes/pdp.php");
    ?>
</body>

</html>