<!DOCTYPE html>
<?php
    session_start();
?>
<html lang="fr" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" type="text/css" href="/css/style.css" media="all" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
    <script defer="defer" type="text/javascript" src="../js/utils-ajax.js"></script>
    <title>Accueil</title>
</head>

<body>
    <?php 
        include("includes/menu.php");
        include("includes/header.php");
        echo '<main>';
            include("pages/acceuil.php");
        echo '</main>';
        include("includes/pdp.php");
    ?>
</body>

</html>